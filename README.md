# jmeter-backend-listener
一个JMeter插件，使您能够将测试结果发送到Kafka服务器。
## 概述

### 描述
JMeter后端监听器Kafka是一个JMeter插件，使您可以将测试结果发送到Kafka服务器。它受JMeter [ElasticSearch后端侦听器插件](https://github.com/delirius325/jmeter-elasticsearch-backend-listener)
的启发。
### 特征

-   筛选器
    -   仅使用过滤器发送所需的样品！只需在相应的字段中按如下所示键入它们即可：filter1;filter2;filter3或sampleLabel_must_contain_this。


-   具体字段
    -   指定要发送到Kafka的字段（以下可能的字段）：
        -   AllThreads
        -   BodySize
        -   Bytes
        -   SentBytes
        -   ConnectTime
        -   ContentType
        -   DataType
        -   ErrorCount
        -   GrpThreads
        -   IdleTime
        -   Latency
        -   ResponseTime
        -   SampleCount
        -   SampleLabel
        -   ThreadName
        -   URL
        -   ResponseCode
        -   TestStartTime
        -   SampleStartTime
        -   SampleEndTime
        -   Timestamp
        -   InjectorHostname

-   模式介绍
    -   **debug** : 发送所有采样器的请求/响应信息（标题，正文等）
    -   **info** : 将所有采样器发送到Kafka服务器，但仅发送标头，失败采样器的主体信息
    -   **quiet** : 仅发送响应时间，字节和其他指标
    -   **error** : 仅将失败的采样器发送到Kafka服务器（附带其标头和主体信息）

-   使用Logstash / NiFi或任何其他工具来使用Kafka主题中的数据，然后将其吸收到您喜欢的数据库中。



### Maven依赖

```xml
<dependency>
    <groupId>io.github.rahulsinghai</groupId>
    <artifactId>jmeter.backendlistener.kafka</artifactId>
    <version>1.0.1</version>
</dependency>
```

### 安装JMeter

-   设置DISPLAY变量：
    ```bash
    export DISPLAY=终端机IP
    ```

-   下载[JMeter](https://jmeter.apache.org/download_jmeter.cgi)
二进制文件并将其解压：

    ```bash
    mkdir -P /home/jmeter
    cd /home/jmeter
    curl -O -k http://mirror.vorboss.net/apache//jmeter/binaries/apache-jmeter-5.1.1.tgz
    tar -zxvf apache-jmeter-5.1.1.tgz
    ln -s apache-jmeter-5.1.1 ./current
    export JMETER_HOME=/data/elastic/jmeter/current
    ```

-   将[插件管理器](https://jmeter-plugins.org/wiki/PluginsManager/)
下载并安装到以下lib/ext文件夹：

    ```bash
    curl -O -k http://search.maven.org/remotecontent?filepath=kg/apc/jmeter-plugins-manager/1.3/jmeter-plugins-manager-1.3.jar
    mv jmeter-plugins-manager-1.3.jar apache-jmeter-5.1.1/lib/ext/
    ```

-   启动JMeter：

    ```bash
    cd $JMETER_HOME
    JVM_ARGS="-Dhttps.proxyHost=myproxy.com -Dhttps.proxyPort=8080 -Dhttp.proxyUser=user -Dhttp.proxyPass=***" ./bin/jmeter.sh
    ```

### 打包和测试您新添加的代码

-   构建：在mvn命令下面执行。确保正确设置了JAVA_HOME

    ```bash
    mvn clean package
    ```

-   将生成的JAR包移至您的JMETER_HOME/lib/ext。

    ```bash
    mv target/jmeter.backendlistener.kafka-1.0.0-SNAPSHOT.jar $JMETER_HOME/lib/ext/
    ```

-   重新启动JMeter

-   转到选项>插件管理器

-   您会在“已安装的插件”选项卡中找到提到的Kafka后端侦听器插件。
### 配置jmeter-backend-listener插件

-   在测试窗格中，右键单击“线程组” >“添加”>“侦听器”>“后端侦听器”
-   选择io.github.rahulsinghai.jmeter.backendlistener.kafka.KafkaBackendClient为Backend Listener Implementation。
-   指定参数，如下图所示（bootstrap.servers和kafka.topic是必选参数）：
![Screenshot of configuration](docs/configuration.JPG "Screenshot of configuration")

### 运行您的JMeter测试计划

您可以使用以下命令以GUI模式或CLI模式运行测试计划：
```bash
bin/jmeter -H [HTTP proxy server] -P [HTTP proxy port] -N "localhost|127.0.0.1|*.singhaiuklimited.com" -n -t test_kafkaserver.jmx -l test_kafkaserver_result.jtl
```

## 截图示例

### Grafana仪表板示例

![Sample Grafana dashboard](https://image.ibb.co/jW6LNx/Screen_Shot_2018_03_21_at_10_21_18_AM.png "Sample Grafana Dashboard")

### 详细介绍

有关更多信息，请参见以下[文档](https://github.com/rahulsinghai/jmeter-backend-listener-kafka/wiki).